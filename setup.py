#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "frutils[cli]==1.0.0b1",
]

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest"]

setup(
    author="Markus Binsteiner",
    author_email="markus@frkl.io",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: Other/Proprietary License",
        "Natural Language :: English",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    description="Utility app to manage a 'freckles' install",
    entry_points={"console_scripts": ["freckles-manager=freckles_manager.cli:main"]},
    install_requires=requirements,
    license="Parity Public License 6.0.0",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="freckles_manager",
    name="freckles_manager",
    packages=find_packages(include=["freckles_manager"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/freckles-io/freckles-manager",
    version="1.0.0b1",
    zip_safe=False,
)
