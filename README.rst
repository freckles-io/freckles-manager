================
freckles-manager
================


.. image:: https://img.shields.io/pypi/v/freckles_manager.svg
           :target: https://pypi.python.org/pypi/freckles_manager
           :alt: pypi

.. image:: https://readthedocs.org/projects/freckles-manager/badge/?version=latest
           :target: https://freckles-manager.readthedocs.io/en/latest/?badge=latest
           :alt: Documentation Status

.. image:: https://gitlab.com/makkus/freckles-manager/badges/develop/pipeline.svg
           :target: https://gitlab.com/makkus/freckles_manager/pipelines
           :alt: pipeline status





.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
           :target: https://github.com/ambv/black
           :alt: codestyle



Utility app to manage a 'freckles' install


* License: Parity Public License 2.1.0
* Documentation: https://freckles-manager.readthedocs.io.


Features
--------

* TODO
