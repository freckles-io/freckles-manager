# -*- coding: utf-8 -*-

"""Main module."""
import importlib
import os
import sys

import click
from plumbum import local, CommandNotFound

FRKL_INDEX_URL = "https://pkgs.frkl.io"
DEV_INDEX = "frkl/dev"
STAGING_INDEX = "frkl/staging"
STABLE_INDEX = "frkl/stable"


def get_install_method():

    try:
        frecklecute = local["frecklecute"]

        path = str(frecklecute)

        if path == os.path.expanduser(
            "~/.local/share/inaugurate/virtualenvs/freckles/bin/frecklecute"
        ):
            return ("virtualenv", False)
        elif path == os.path.expanduser(
            "~/.local/share/inaugurate/conda/envs/freckles/bin/frecklecute"
        ):
            return ("conda", False)

    except (CommandNotFound) as cnf:
        raise Exception(
            "Can't determine 'freckles' installation. Please update manually: {}".format(
                cnf
            )
        )


def upgrade_pip():

    pip = local[get_pip_path()]
    command = ["install", "--upgrade", "pip"]

    click.echo("Upgrading pip...")
    rc, stdout, stderr = pip.run(command)

    if rc != 0:
        raise Exception("Could not upgrade pip: {}".format(stderr))

    return stdout


def get_pip_path():

    python_path = sys.executable
    pip_path = os.path.join(os.path.dirname(python_path), "pip")

    return pip_path


PACKAGES_TO_UPDATE = [
    "frutils",
    "frkl",
    "ting",
    "nsbl",
    "freckles",
    "freckles-cli",
    "freckles-adapter-nsbl",
    "templing",
]


class FrecklesManager(object):
    def __init__(self):

        self.install_method = get_install_method()

    def update_manager(self):

        pip = local[get_pip_path()]

        index = DEV_INDEX

        command = [
            "install",
            "--pre",
            "--extra-index-url",
            "{}/{}".format(FRKL_INDEX_URL, index),
            "--force-reinstall",
            "--upgrade-strategy",
            "only-if-needed",
            "--upgrade",
            "freckles-manager",
        ]

        click.echo("Installing/Updating freckles-manager...")
        rc, stdout, stderr = pip.run(command)

        if True or rc != 0:
            print(stdout)
            print("---------")
            print(stderr)

            sys.exit(1)
        else:
            click.echo("Success.")

    def update(self, path_or_name=None, force_reinstall=False):

        # upgrade_pip()

        pip = local[get_pip_path()]

        if force_reinstall:

            command = ["uninstall", "-y"]
            for p in PACKAGES_TO_UPDATE:
                command.append(p)

            click.echo("Uninstalling freckles...")
            rc, stdout, stderr = pip.run(command)
            if True or rc != 0:
                print(stdout)
                print("---------")
                print(stderr)
            else:
                click.echo("Success.")

        index = DEV_INDEX

        force_reinstall_basic = False

        command = [
            "install",
            "--pre",
            "--extra-index-url",
            "{}/{}".format(FRKL_INDEX_URL, index),
            "--upgrade-strategy",
            "only-if-needed",
            "--upgrade",
        ]

        path = []

        if not path_or_name:
            path = PACKAGES_TO_UPDATE

        else:
            path = []
            force_reinstall_basic = True
            p = os.path.realpath(path_or_name)

            if os.path.exists(p):
                if os.path.isdir(p):
                    path.append("-e")
                path.append(p)

        if force_reinstall_basic:
            command.append("--force-reinstall")

        command.extend(path)

        click.echo("Installing/Updating freckles...")
        rc, stdout, stderr = pip.run(command)

        if True or rc != 0:
            print(stdout)
            print("---------")
            print(stderr)

            sys.exit(1)
        else:
            click.echo("Success.")

        self.update_manager()

        globals()["freckles"] = importlib.import_module("freckles")
