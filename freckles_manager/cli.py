# -*- coding: utf-8 -*-

"""Console script for freckles_manager."""
import sys
import click

from .freckles_manager import FrecklesManager


@click.command()
@click.option(
    "--force",
    "-f",
    help="reinstall 'freckles' and its dependencies, even if it is up-to-date",
    is_flag=True,
)
def main(force):
    """Manages your freckles installation."""

    manager = FrecklesManager()
    manager.update(force_reinstall=force)
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
